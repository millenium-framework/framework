Millenium ~ Framework
------

## About
- PHP MVC фреймворк.

## Installation
1. Создать __Config.yml__.
2. Создать __Routing.yml__.
3. Создать __index.php__(точка входа в проект).

## Configuration
1. Файл конфигурации:
```yaml
Project: # Свойства проекта
  name: Again # название

Route: # Роутинг
  page: Config/Routing.yml # путь до файла с роутингами

Templater: # Шаблонизатор
  name: Fenom # название шаблонизатора(должно совпадать с сущностью)
  templates_dir: templates/ # путь директории с шаблонами
  templates_c_dir: templates_c/ # путь директории с кешированными шаблонами

Views: # Схемы
  prefix: Views/ # директория со схемами

  # Список схем
  menu: Menu.yml

Storage: # Хранилище сущностей
  path: Storage # путь до библиотеки сущностей
```
2. Routing.yml
```yaml
index: # Главная страница.
    url: /
    class: %namespace%\Controllers\Index
```
3. index.php:
```php
define('__ROOT__', __DIR__ . '/'); # Путь до корня проекта

require_once __ROOT__ . 'vendor/autoload.php'; # Подключение автозагрузки composer'а

$bootstrap = new \Millenium\Framework\Bootstrap(); # Начинаем работу
$bootstrap
    ->setConfigFile(%путь_до_конфига%)
    ->init();
```
