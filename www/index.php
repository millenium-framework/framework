<?php

/**
 * Начальная страница проекта.
 *
 * @package     Millenium\Framework
 * @author      Alexandr Golikov <my-millenium@yandex.ru>
 * @copyright   2015 Alexandr Golikov
 */
session_start();

define('__ROOT__', dirname(__DIR__) . '/');
set_include_path(get_include_path() . PATH_SEPARATOR . __ROOT__);

require_once 'vendor/autoload.php';

$bootstrap = new \Millenium\Framework\Bootstrap();
$bootstrap
    ->setConfigFile('src/Config/Project.yml')
    ->init();
