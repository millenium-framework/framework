Директория для конфигов, настроек и т.п проекта.

# Пример одного роута: #
```yaml
index:
    url: /
    class: Millenium\Framework\Controllers\Index
```

# Пример конфига: #
```yaml
# Роутинг
Route:
  page: src/Config/Routing.yml

# Шаблонизатор
Templater:
  name: Fenom
  templates_dir: templates/
  templates_c_dir: templates_c/
```