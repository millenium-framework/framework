<?php

namespace Millenium\Framework\Controllers\Acp;

use \Millenium\Framework\Controllers;

/**
 * Базовая страница админки.
 *
 * @package     Millenium\Frameworks\Controller\Admin
 * @author      Alexandr Golikov <my-millenium@yandex.ru>
 * @copyright   2016 Alexandr Golikov
 */
abstract class Base extends Controllers\Base
{

    /**
     * Доступные группы для допуска.
     *
     * @var array
     */
    protected $availableGroups = [
        'admin'
    ];

    /**
     * Действия перед работой основного действия.
     *
     * @return $this
     */
    protected function beforeAction()
    {
        $groups = $this->getConfig()->getSession('groups');
        $user = $this->getRegistry()->get('Users');

        if (!$user->isAuth()) {
            header('Location: /');

            exit();
        }

        foreach ($this->availableGroups as $value) {
            if (!in_array($value, $groups)) {
                header('Location: /');

                exit();
            }
        }

        $vars = [
            // Если авторизован, то логин пользователя, иначе false.
            'login' => $this->getConfig()->getSession('login'),
            'groups' => is_array($groups) ? array_flip($groups) : [],
            'pageUrl' => strstr($_SERVER['REQUEST_URI'], '?', true),
            'menu' => $this->getRegistry()->get('Views', 'Menu')->getCompleteView()
        ];

        $this->fenom()->assignAll($vars);

        return $this;
    }
}
