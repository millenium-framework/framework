<?php

namespace Millenium\Framework\Controllers\Api;

use \Millenium\Framework\Controllers;

/**
 * Абстрактный класс для API.
 *
 * @package     Millenium\Framework\Controllers\Api
 * @author      Alexandr Golikov <my-millenium@yandex.ru>
 * @copyright   2016 Alexandr Golikov
 */
abstract class Base extends Controllers\Base implements ApiInterface
{

    /**
     * Основной метод.
     * Определяем HTTP метод и на основе этого отдаем информацию.
     *
     * @return mixed
     */
    public function main()
    {
        switch ($_SERVER['REQUEST_METHOD']) {
            case 'OPTIONS':
                $requestMethod = 'OPTIONS';
                break;
            case 'GET':
                $requestMethod = 'GET';
                break;
            case 'HEAD':
                $requestMethod = 'HEAD';
                break;
            case 'POST':
                $requestMethod = 'POST';
                break;
            case 'PUT':
                $requestMethod = 'PUT';
                break;
            case 'PATCH':
                $requestMethod = 'PATCH';
                break;
            case 'TRACE':
                $requestMethod = 'TRACE';
                break;
            case 'CONNECT':
                $requestMethod = 'CONNECT';
                break;
            case 'DELETE':
                $requestMethod = 'DELETE';
                break;
        }

        if (!in_array($requestMethod, $this->allowMethod)) {
            return $this->methodNotAllowed();
        }

        return $this->$requestMethod();
    }

    /**
     * Возрашаем ошибку "метод не доступен" и список доступных методов.
     *
     * @return string
     */
    public function methodNotAllowed()
    {
        $data = [
            'code' => 405,
            'description' => 'Метод не доступен.'
        ];

        $this
            ->setResponceCode(405)
            ->setHeader('Allow: ' . implode(', ', $this->allowMethod));

        return json_encode($data);
    }

    /**
     * Действия перед основным действием.
     *
     * @return $this
     */
    public function beforeAction()
    {
        $this->setHeader('Content-Type: application/json; charset=utf-8');

        return $this;
    }

    /**
     * Действие для HTTP метода HEAD.
     *
     * @return void
     */
    public function head()
    {
        $this->setHeader('Content-Type: text/html; charset=utf-8');

        return $this;
    }

    /**
     * Действие для HTTP метода options.
     *
     * @return void
     */
    public function options()
    {
        return $this;
    }

    /**
     * Действие для HTTP метода get.
     *
     * @return void
     */
    public function get()
    {
        return $this;
    }

    /**
     * Действие для HTTP метода post.
     *
     * @return void
     */
    public function post()
    {
        return $this;
    }

    /**
     * Действие для HTTP метода put.
     *
     * @return void
     */
    public function put()
    {
        return $this;
    }

    /**
     * Действие для HTTP метода patch.
     *
     * @return void
     */
    public function patch()
    {
        return $this;
    }

    /**
     * Действие для HTTP метода delete.
     *
     * @return void
     */
    public function delete()
    {
        return $this;
    }
}
