<?php

namespace Millenium\Framework\Controllers\Api;

/**
 * Интерфейс HTTP методов для API.
 *
 * @package     Millenium\Framework\Controllers\Api
 * @author      Alexandr Golikov <my-millenium@yandex.ru>
 * @copyright   2016 Alexandr Golikov
 */
interface ApiInterface
{

    /**
     * [head description]
     *
     * @return [type] [description]
     */
    public function head();

    /**
     * [head description]
     *
     * @return [type] [description]
     */
    public function options();

    /**
     * [head description]
     *
     * @return [type] [description]
     */
    public function get();

    /**
     * [head description]
     *
     * @return [type] [description]
     */
    public function post();

    /**
     * [head description]
     *
     * @return [type] [description]
     */
    public function put();

    /**
     * [head description]
     *
     * @return [type] [description]
     */
    public function patch();

    /**
     * [head description]
     *
     * @return [type] [description]
     */
    public function delete();
}
