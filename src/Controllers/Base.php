<?php

namespace Millenium\Framework\Controllers;

use \Millenium\Framework;

/**
 * Базовый контроллер.
 * Наследуется всеми контроллерами.
 *
 * @package     Millenium\Framework\Controllers
 * @author      Alexandr Golikov <my-millenium@yandex.ru>
 * @copyright   2016 Alexandr Golikov
 */
abstract class Base extends Framework\Base
{

    /**
     * Действие(метод) класса для вызова.
     *
     * @var string
     */
    protected $action = 'main';
}
