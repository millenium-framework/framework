<?php

namespace Millenium\Framework;

use \Millenium\Framework;
use \Millenium\Framework\Exceptions;

/**
 * Подготовка окружения и выполнение.
 *
 * @package     Millenium\Framework
 * @author      Alexandr Golikov <my-millenium@yandex.ru>
 * @copyright   2015 Alexandr Golikov
 */
class Bootstrap
{

    /**
     * [$confifFile description]
     *
     * @var string
     */
    private $configFile;

    /**
     * [FunctionName description]
     *
     * @param [type] $configFile [description]
     */
    public function __construct($configFile)
    {
        if (!file_exists($configFile)) {
            throw new Exceptions\Framework(
               'Передан некорректный конфигурационный файл'
            );
        }

        $this->configFile = $configFile;

        return $this->init();
    }

    /**
     * [proceed description]
     *
     * @return [type] [description]
     */
    private function init()
    {
        try {
            return $this->proceed();
        } catch (Exceptions\Config $e) {
            error_log($e->getMessage());
        } catch (Exceptions\Registry $e) {
            error_log($e->getMessage());
        } catch (\MongoDB\Driver\Exception\BulkWriteException $e) {
            $writeResult = $e->getWriteResult();

            if ($writeConcernError = $writeResult->getWriteConcernError()) {
                var_dump($writeConcernError);
            }

            if ($writeErrors = $writeResult->getWriteErrors()) {
                $writeErrors = reset($writeErrors);
                echo $writeErrors->getMessage();
            }
        } catch (Exceptions\Routing $e) {
            error_log($e->getMessage());
        }

        echo 'Ooops. Something bad happened. Please, come back later.';
        exit();
    }

    /**
     * [do description]
     *
     * @return [type] [description]
     */
    private function proceed()
    {
        if (session_status() === 1) {
            session_start();
        }

        $config = new Framework\Config($this->configFile);
        $routing = new Framework\Routing($config);

        $di = new Framework\Registry\Di();
        $di
            ->setConfig($config)
            ->init();

        $application = $routing->findRoute();
        $application
            ->setRegistry($di)
            ->setConfig($config)
            ->initApplication();

        echo $application;

        return true;
    }
}
