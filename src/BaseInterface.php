<?php

namespace Millenium\Framework;

use \Millenium\Framework\Registry;

/**
*
*/
interface BaseInterface
{
    public function __toString();

    public function initApplication();

    public function setAction($action);

    public function setRegistry(Registry\Di $registry);

    public function getRegistry();

    public function setConfig(Config $config);

    public function getConfig();

    public function getTemplater();
}
