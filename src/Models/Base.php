<?php

namespace Millenium\Framework\Models;

use \Millenium\Framework;

/**
 * Базовая модель.
 *
 * @package     Millenium\Framework\Models
 * @author      Alexandr Golikov <my-millenium@yandex.ru>
 * @copyright   2016 Alexandr Golikov
 */
class Base extends Framework\Base
{
}
