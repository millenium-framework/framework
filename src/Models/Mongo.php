<?php

namespace Millenium\Framework\Models;

use Millenium\Framework\Datasource;

/**
 * Модель MongoDB.
 *
 * @package     Millenium\Framework\Model
 * @author      Alexandr Golikov <my-millenium@yandex.ru>
 * @copyright   2016 Alexandr Golikov
 */
class Mongo extends Datasource\Mongo
{

    /**
     * Название коллекции.
     *
     * @var string
     */
    protected $collectionName = '';

    /* Create */

    /**
     * Делаем новую запись.
     *
     * @param array $data данные для запроса
     *
     * @return $this
     */
    public function create(array $data)
    {
        $data['_id'] = $this->getAutoincrement();

        $db = $this->_config->getConfigValue('Mongo', 'db');

        $this->insertOne($data, $this->collectionName, $db);

        return $this;
    }

    /**
     * Создаем одну запись.
     *
     * @param array $data массив с информаций для создания
     *
     * @return object $this
     */
    public function createOne(array $data)
    {
        $data['_id'] = $this->getAutoincrement();

        $db = $this->_config->getConfigValue('Mongo', 'db');

        $this->insertOne($data, $this->collectionName, $db);

        return $this;
    }

    /**
     * Обычная запись и запись в коллекцию поиска.
     *
     * @param array $data данные для запроса
     *
     * @return $this
     */
    public function createForSearch(array $data)
    {
        $data['_id'] = $this->getAutoincrement($this->collectionName);

        $db = $this->_config->getConfigValue('Mongo', 'db');
        $this->insertOne($data, $this->collectionName, $db);

        $searchData = [
            '_id' => $this->getAutoincrement('search'),
            'data' => [
                'origin_id' => $data['_id'],
                'name' => $data['name']
            ],
            'collection' => $this->collectionName
        ];

        $data['collection'] = $this->collectionName;

        $memcached = $this->_registry->get('memcached');
        $memcached->set($this->collectionName . '_' . $data['_id'], $data);

        $this->insertOne($searchData, 'search', $db);

        return $this;
    }

    /* Read*/

    /**
     * Получить одну запись.
     *
     * @param array $data данные для запроса
     * @param array $options опции для запроса
     *
     * @return array
     */
    public function getOne(array $data = [], array $options = [])
    {
        $result = $this->_getData($data, $options);

        return reset($result);
    }

    /**
     * Получаем все записи.
     *
     * @param array $data данные для запроса
     * @param array $options опции для запроса
     *
     * @return array
     */
    public function getAll(array $data = [], array $options = [])
    {
        return $this->_getData($data, $options);
    }

    /**
     * Получить все записи и их количество.
     *
     * @param array $data данные для запроса
     * @param array $options опции для запроса
     *
     * @return array
     */
    public function getAllWithCount(array $data = [], array $options = [])
    {
        $result = [
            'data' => false,
            'count' => 0
        ];

        $result['data'] = $this->getAll($data, $options);

        if (!empty($result['data'])) {
            $result['count'] = count($result['data']);
        }

        return $result;
    }

    /**
     * Получить количество записей.
     *
     * @param array $data запрос по которому получаем количество записей
     *
     * @return integer
     */
    public function getCount(array $data = [])
    {
        $count = $this->_command(
            $this->_config->getConfigValue('Mongo', 'db'),
            [
                'count' => $this->collectionName,
                'query' => $data,
            ]
        );

        return intval($count->n);
    }

    /**
     * Получаем данные из базы.
     *
     * @param array $data запрос по которому достаем данные
     * @param array $options опции запроса
     *
     * @return array результат запроса
     */
    private function _getData(array $data, array $options = [])
    {
        $data = $this->_prepareQuery($data, $options);

        $db = $this->_config->getConfigValue('Mongo', 'db');
        $result = $this->executeQuery($db . '.' . $this->collectionName, $data);

        // Волшебное преобразование из stdClass в array.
        $result->setTypeMap(
            [
                'root' => 'array',
                'document' => 'array'
            ]
        );

        $result = $result->toArray();

        return $result;
    }

    /* Update */

    /**
     * Обновляем одну запись.
     */
    public function updateOne(array $query, array $updates, $upsert = false, $multi = false)
    {
        $db = $this->_config->getConfigValue('Mongo', 'db');

        return $this->_updateOne(
            $query,
            $updates,
            $upsert,
            $multi,
            $this->collectionName,
            $db
        );
    }

    /**
     * Обновляем множество записей.
     *
     * @param array $data массив с запросом по которому производим обновление
     * @param array $options опции запроса
     */
    public function updateMany(array $data, array $options)
    {
    }

    /* Delete */

    /**
     * Удаляем записи из базы.
     *
     * @param array $data данные для запроса
     * @param array $options опции для запроса
     *
     * @return object $this
     */
    public function delete(array $data, array $params = null)
    {
        $data = $this->remove($data);

        $db = $this->_config->getConfigValue('Mongo', 'db');
        $this->executeBulkWrite($db . '.' . $this->collectionName, $data);

        return $this;
    }

    /* Other */

    /**
     * Получаем автоинкремент записи в коллекции.
     * Возвращает следующий возможный номер.
     * Если значение для коллекции отсутствовало, то запускаем себя еще раз.
     *
     * @param string $collectionName название коллекции для которой вернуть автоинкремент
     *
     * @return integer
     */
    public function getAutoincrement($collectionName = false)
    {
        if (empty($collectionName)) {
            $collectionName = $this->collectionName;
        }

        $sequence = $this->_command(
            $this->_config->getConfigValue('Mongo', 'db'),
            [
                'findandmodify' => 'counters',
                'query' => [
                    '_id'=> $collectionName
                ],
                'update' => [
                    '$inc' => [
                        'count' => 1
                    ]
                ],
                'upsert' => true
            ]
        );

        if (!empty($sequence)) {
            if (!is_null($sequence->value)) {
                return $sequence->value->count;
            }

            return $this->getAutoincrement($this->collectionName);
        }

        return false;
    }

    /**
     * Подготовка группового запроса.
     *
     * @param array $data данные для запроса
     *
     * @return object \MongoDB\Driver\Query
     */
    private function _prepareQuery(array $data, array $options = [])
    {
        return $this->createQuery($data, $options);
    }
}
