<?php

namespace Millenium\Framework\Models;

use Millenium\Framework\Datasource;

/**
 * Модель memcached.
 *
 * @package     Millenium\Framework\Models
 * @author      Alexandr Golikov <my-millenium@yandex.ru>
 * @copyright   2016 Alexandr Golikov
 */
class Memcached extends Datasource\Memcached
{

    /**
     * Экземпляр memcached.
     *
     * @var \Memcached
     */
    protected $memcached;

    /**
     * Инициализация memcached.
     *
     * @return $this
     */
    public function init()
    {
        if (empty($this->memcached)) {
            $config = $this->getConfig();

            $server = $config->getConfigValue('Memcached', 'server');
            $port = $config->getConfigValue('Memcached', 'port');

            $memcached = new \Memcached();
            $memcached->addServer($server, $port);

            $this->memcached = $memcached;
        }

        return $this;
    }

    /**
     * Установка массива значений.
     *
     * @param array $data массив со значениями для установки в кеш
     *
     * @return $this
     */
    public function setArray(array $data)
    {
        foreach ($data as $key => $value) {
            $this->set($key, $value);
        }

        return $this;
    }
}
