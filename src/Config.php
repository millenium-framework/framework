<?php

namespace Millenium\Framework;

use \Symfony\Component\Yaml\Parser;

/**
 * Класс конфигурации.
 * Отвечает за получение данных из файла с конфигом проекта.
 *
 * @package     Millenium\Framework
 * @author      Alexandr Golikov <my-millenium@yandex.ru>
 * @copyright   2016 Alexandr Golikov
 */
class Config
{

    /**
     * Файл конфига.
     *
     * @var string
     */
    private $configFile = '';

    /**
     * Переменные из URL.
     *
     * @var string
     */
    private $urlVars = [];

    /**
     * Конструктор.
     *
     * @param string $confFile путь до файла с конфигом
     */
    public function __construct($confFile)
    {
        if (empty($confFile)) {
            throw new \Millenium\Framework\Exceptions\Config(
                'Не передан файл с конфигом проекта'
            );
        }

        $this->configFile = __ROOT__ . $confFile;

        return $this;
    }

    /**
     * Получаем значение из конфига.
     *
     * @param string $category категория в конфиге
     * @param string $value значение в категории
     *
     * @return string
     */
    public function getConfigValue($category, $value = null)
    {
        if (!file_exists($this->configFile)) {
            throw new \Millenium\Framework\Exceptions\Config(
                'Не найден файл(' . $this->configFile . ') с конфигом проекта'
            );
        }

        $category = ucfirst(strtolower($category));
        $value = strtolower($value);

        $parser = new Parser();
        $configFile = $parser->parse(file_get_contents($this->configFile));

        if (!isset($configFile[$category])) {
            throw new \Millenium\Framework\Exceptions\Config(
                'Указанная категория(' . $category . ') в конфиге не существует'
            );
        }

        if (empty($value)) {
            return $configFile[$category];
        }

        if (!isset($configFile[$category][$value])) {
            throw new \Millenium\Framework\Exceptions\Config(
                'Указанного значения(' . $value . ') нет в указанной категории'
            );
        }

        if (isset($configFile[$category]['prefix'])) {
            return $configFile[$category]['prefix'] . $configFile[$category][$value];
        }

        return $configFile[$category][$value];
    }

    /**
     * Устанавливаем переменные из URL.
     *
     * @param array $variables
     */
    public function setUrlVars(array $variables)
    {
        $this->urlVars = $variables;

        return $this;
    }

    /**
     * Получаем переменную из URL.
     *
     * @param string $name
     *
     * @return string|boolean
     */
    public function getUrlVar($name)
    {
        if (isset($this->urlVars[$name])) {
            return $this->urlVars[$name];
        }

        return false;
    }
}
