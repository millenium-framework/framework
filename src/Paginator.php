<?php

namespace Millenium\Framework;

/**
 * Пагинатор.
 *
 * @package     Millenium\Framework
 * @author      Alexandr Golikov <my-millenium@yandex.ru>
 * @copyright   2016 Alexandr Golikov
 */
class Paginator
{

    /**
     * Номер запрошенной страницы.
     *
     * @var integer
     */
    private $page = 1;

    /**
     * Сколько эллементов на странице.
     *
     * @var integer
     */
    private $limit = 12;

    /**
     * Конструктор.
     *
     * @param integet $limit количество эллементов для отображения
     * @param integet $page номер страницы для отображения
     *
     * @return $this
     */
    public function __construct($limit = false, $page = false)
    {
        if (!empty($_GET['limit'])) {
            $this->limit = (int)$_GET['limit'];
        } elseif (!empty($limit)) {
            $this->limit = (int)$limit;
        }

        if (!empty($_GET['page'])) {
            $this->page = (int)$_GET['page'];
        } elseif (!empty($page)) {
            $this->page = (int)$page;
        }

        return $this;
    }

    /**
     * Основной метод.
     *
     * @param array|integer $data массив с запросом или число эллементов
     *
     * @return array массив со всей необходимой информацией для работы пагинатора
     */
    public function getPagination($data)
    {
        // Общее количество.
        if (is_array($data)) {
            $totalItems = count($data);
        } elseif (is_numeric((int)$data)) {
            $totalItems = $data;
        } else {
            throw new \Millenium\Framrwork\Paginator(
                'Can\'t extract number from $data for paginating.
            ');
        }

        if ($this->limit > 24) {
            $this->limit = 24;
        }

        // Вычисляем общее количество страниц.
        $totalPages = (int)ceil($totalItems / $this->limit);

        // Текущая страница.
        $currentPage = $this->page;

        // Небольшая защита от ошибочных запросов.
        if ($this->page > $totalPages) {
            $currentPage = $totalPages;
        } elseif ($this->page < 1) {
            $currentPage = 1;
        }

        // Вычисляем сколько элементов пропустить.
        $skip = ($this->limit * $currentPage) - $this->limit;

        // Легкая магия с адресной строкой.
        $pageUrl = $_SERVER['REQUEST_URI'];

        if (strstr($pageUrl, 'page')) {
            $pageUrl = preg_replace('#\?*&*page=\d#', '', $pageUrl);
        }

        if (strstr($pageUrl, '?')) {
            $pageUrl = $pageUrl . '&page=';
        } else {
            $pageUrl = $pageUrl . '?page=';
        }

        return [
            'skip' => $skip > 0 ? (int)$skip : 0,
            'limit' => (int)$this->limit,
            'previousPage' => $currentPage > 1 ? (int)$currentPage - 1 : false,
            'currentPage' => (int)$currentPage,
            'nextPage' => $currentPage < $totalPages ? (int)$currentPage + 1 : false,
            'totalPages' => (int)$totalPages,
            'totalItems' => (int)$totalItems,
            'pageUrl' => $pageUrl
        ];
    }
}
