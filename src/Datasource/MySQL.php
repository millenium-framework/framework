<?php

namespace Application\Datasource;

/**
 * MySQL datasource.
 *
 * @package     Millenium\Framework\Datasource
 * @author      Alexandr Golikov <my-millenium@yandex.ru>
 * @copyright   2016 Alexandr Golikov
 */
class MySQL extends Base
{

    /**
     * MySQL instance.
     *
     * @var object \mysqli
     */
    private $mysqli;

    /**
     * Array with data for future query.
     *
     * @var array
     */
    private $query = [];

    /**
     * Ger instance of MySQL.
     *
     * @return object
     */
    public function initConnect()
    {
        if (empty($this->mysqli)) {
            $config = $this->getConfig();

            $host = $config->getConfigValue('MySQL', 'host');
            $user = $config->getConfigValue('MySQL', 'user');
            $password = $config->getConfigValue('MySQL', 'password');
            $db = $config->getConfigValue('MySQL', 'db');

            $this->mysqli = new \mysqli($host, $user, $password, $db);

            if ($this->mysqli->connect_error) {
                throw new \Millenium\Framework\Exceptions\Database(
                    'Ошибка подключения (' . $mysql->connect_errno . ') ' . $mysql->connect_error
                );
            }

            $this->mysqli->query('set names utf8');
        }

        return $this;
    }

    /**
     * Get complete query string.
     *
     * @return string
     */
    public function getQuery()
    {
        if (empty($this->query['type'])) {
            throw new \Millenium\Framework\Exceptions\MySQL('Empty query');
        }

        $query = $this->query['type'];

        if (!empty($this->query['fields'])) {
            if (is_string($this->query['fields'])) {
                $query .= ' ' . $this->query['fields'];
            } elseif (is_array($this->query['fields'])) {
                $query .= ' ' . implode(',', $this->query['fields']);
            } else {
                throw new \Millenium\Framework\Exceptions\MySQL('Wrong query fields');
            }
        }

        if (empty($this->query['table'])) {
            throw new \Millenium\Framework\Exceptions\MySQL('Empty table');
        }

        switch ($this->query['type']) {
            case 'SELECT':
                $query .= ' FROM ' . $this->query['table'];
                break;
            case 'DELETE':
                $query .= ' FROM ' . $this->query['table'];
                break;
            case 'UPDATE':
                $query .= ' ' . $this->query['table'];
                break;
            case 'INSERT INTO':
                $query .= ' ' . $this->query['table'];
                break;
            case 'SHOW COLUMNS':
                $query .= ' FROM ' . $this->query['table'];
                break;
            default:
                throw new \Millenium\Framework\Exceptions\MySQL('Wrong query type');
                break;
        }

        if (!empty($this->query['valuesForInsert'])) {
            $query .= $this->query['valuesForInsert'];
        }

        if (!empty($this->query['valuesForUpdate'])) {
            $end = end(array_flip($this->query['valuesForUpdate']));
            $query .= ' SET';

            foreach ($this->query['valuesForUpdate'] as $key => $value) {
                $query .= ' ' . $key . ' = "' . $value . '"';

                if ($end != $key) {
                    $query .= ',';
                }
            }
        }

        if (isset($this->query['needWhere'])) {
            $query .= ' WHERE 1=1';
            if (!empty($this->query['where'])) {
                foreach ($this->query['where'] as $key => $value) {
                    $query .= ' AND ' . $key . ' = "' . $value . '"';
                }
            }

            if (!empty($this->query['whereIn'])) {
                foreach ($this->query['whereIn'] as $whereInKey => $whereInValue) {
                    $query .= ' AND ' . $whereInKey . ' IN (' . implode(',', $whereInValue) . ')';
                }
            }

            if (!empty($this->query['whereIsNull'])) {
                foreach ($this->query['whereIsNull'] as $key => $value) {
                    $query .= ' AND "' . $value . '" IS NULL';
                }
            }

            if (!empty($this->query['whereIsNotNull'])) {
                foreach ($this->query['whereIsNotNull'] as $key => $value) {
                    $query .= ' AND `' . $value . '` IS NOT NULL';
                }
            }
        }

        if (!empty($this->query['order'])) {
            $query .= ' ORDER BY ' . $this->query['order'];

            if (!empty($this->query['desc'])) {
                $query .= ' DESC';
            } else {
                $query .= ' ASC';
            }
        }

        if (!empty($this->query['limit'])) {
            $query .= ' LIMIT ' . $this->query['limit'];
        }

        if (!empty($this->query['offset'])) {
            $query .= ' OFFSET ' . $this->query['offset'];
        }

        return $query;
    }

    /**
     * Reset query.
     * If need execute more than one query need reset previous query string.
     *
     * @return object $this
     */
    public function resetQuery()
    {
        $this->query = [];

        return $this;
    }

    /**
     * Execute query.
     *
     * @return boolean|object mysqli_result
     */
    public function executeQuery($rawResponce = false)
    {
        $data = $this->mysqli->query($this->getQuery());

        if (!empty($this->mysqli->error)) {
            throw new \Millenium\Framework\Exceptions\MySQL($this->mysqli->error);
        }

        $this->resetQuery();

        $result = [];

        if ($rawResponce == true) {
            return $data;
        }

        while ($row = $data->fetch_assoc()) {
            $result[] = $row;
        }

        return $result;
    }

    /**
     * Set query type.
     *
     * @param string $type
     *
     * @return object $this
     */
    public function setType($type)
    {
        $this->query['type'] = $type;

        return $this;
    }

    /**
     * Set query fields.
     *
     * @param string|array $fields
     *
     * @return object $this
     */
    public function setFields($fields)
    {
        $this->query['fields'] = $fields;

        return $this;
    }
    /**
     * Set query table.
     *
     * @param string $table
     *
     * @return object $this
     */
    public function setTable($table)
    {
        $this->query['table'] = $table;

        return $this;
    }

    /**
     * Set query values for insert.
     *
     * @param array $fields
     * @param array $values
     *
     * @return object $this
     */
    public function setValuesForInsert(array $fields, array $values)
    {
        $values = array_map(function(&$value) {return '"' . $value . '"';}, $values);

        $result = '(' . implode(',', $fields) . ') VALUES (' . implode(',', $values) . ')';

        $this->query['valuesForInsert'] = $result;

        return $this;
    }

    /**
     * Set query values for update.
     *
     * @param array $values
     *
     * @return object $this
     */
    public function setValuesForUpdate(array $values)
    {
        $this->query['valuesForUpdate'] = $values;

        return $this;
    }

    /**
     * Set query where.
     *
     * @param array $where
     *
     * @return object $this
     */
    public function setWhere(array $where)
    {
        $this->query['needWhere'] = true;

        $this->query['where'] = $where;

        return $this;
    }

    /**
     * Set query whereIn.
     *
     * @param array $whereIn
     *
     * @return object $this
     */
    public function setWhereIn(array $whereIn)
    {
        $this->query['needWhere'] = true;

        $this->query['whereIn'] = $whereIn;

        return $this;
    }

    /**
     * Set query whereIsNull.
     *
     * @param array $whereIn
     *
     * @return object $this
     */
    public function setWhereIsNull(array $whereIsNull)
    {
        $this->query['needWhere'] = true;

        $this->query['whereIsNull'] = $whereIsNull;

        return $this;
    }

    /**
     * Set query whereIn.
     *
     * @param array $whereIn
     *
     * @return object $this
     */
    public function setWhereIsNotNull(array $whereIsNotNull)
    {
        $this->query['needWhere'] = true;

        $this->query['whereIsNotNull'] = $whereIsNotNull;

        return $this;
    }

    /**
     * Set query limit.
     *
     * @param integer $limit
     *
     * @return object $this
     */
    public function setLimit($limit)
    {
        $this->query['limit'] = $limit;

        return $this;
    }

    /**
     * Set query offset.
     *
     * @param integer $offset
     *
     * @return object $this
     */
    public function setOffset($offset)
    {
        $this->query['offset'] = $offset;

        return $this;
    }

    /**
     * Set query order.
     *
     * @param string $order
     *
     * @return object $this
     */
    public function setOrder($order)
    {
        $this->query['order'] = $order;

        return $this;
    }

    /**
     * Set query desc.
     *
     * @param boolean $desc
     *
     * @return object $this
     */
    public function setDesc($desc = false)
    {
        $this->query['desc'] = $desc;

        return $this;
    }

    /**
     * Get insert id.
     *
     * @return integer
     */
    public function getInsertId()
    {
        return $this->mysqli->insert_id;
    }

    /**
     * [beginTransaction description]
     *
     * @return [type] [description]
     */
    public function beginTransaction()
    {
        $this->mysqli->begin_transaction(MYSQLI_TRANS_START_READ_WRITE);

        return $this;
    }

    /**
     * [endTransaction description]
     *
     * @return [type] [description]
     */
    public function commitTransaction()
    {
        $this->mysqli->commit();

        return $this;
    }

    /**
     * [rollbackTransaction description]
     *
     * @return [type] [description]
     */
    public function rollbackTransaction()
    {
        $this->mysqli->rollback();

        return $this;
    }
}
