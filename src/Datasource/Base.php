<?php

namespace Millenium\Framework\Datasource;

use \Millenium\Framework;

/**
 * Базовый класс для поставщиков данных.
 *
 * @package     Millenium\Framework\Datasource
 * @author      Alexandr Golikov <my-millenium@yandex.ru>
 * @copyright   2015 Alexandr Golikov
 */
abstract class Base extends Framework\Base
{
}
