<?php

namespace Millenium\Framework\Datasource;

/**
 * Поставщик данных. Memcached.
 *
 * @package     Millenium\Framework\Datasource
 * @author      Alexandr Golikov <my-millenium@yandex.ru>
 * @copyright   2016 Alexandr Golikov
 */
class Memcached extends Base
{

    /**
     * Устанавливаем значение.
     *
     * @param string $key имя ключа
     * @param mixed $value значение
     * @param int $expiration время жизни
     *
     * @return $this
     */
    public function set($key, $value, $expiration = 0)
    {
        return $this->memcached->set($key, $value, $expiration);
    }

    /**
     * Получаем значение по ключу.
     *
     * @param string $key
     *
     * @return mixed
     */
    public function get($key)
    {
        return $this->memcached->get($key);
    }

    /**
     * Удаляем значение по ключу.
     *
     * @param string $key
     *
     * @return mixed
     */
    public function delete($key, $time = 0)
    {
        return $this->memcached->delete($key, $time);
    }

    /**
     * Получаем все ключи.
     *
     * @return array
     */
    public function getAllKeys()
    {
        return $this->memcached->getAllKeys();
    }
}
