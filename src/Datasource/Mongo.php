<?php

namespace Application\Datasource;

/**
 * Поставщик данных. MongoDB.
 *
 * @package     Millenium\Framework\Datasource
 * @author      Alexandr Golikov <my-millenium@yandex.ru>
 * @copyright   2016 Alexandr Golikov
 */
class Mongo extends Base
{

    /**
     * Экземпляр MongoDB.
     *
     * @var null|object \MongoDB\Driver\Manager
     */
    protected $manager = null;

    /**
     * Экземпляр \MongoDB\Driver\BulkWrite
     *
     * @var null|object \MongoDB\Driver\BulkWrite
     */
    protected $bulkWrite = null;

    /**
     * Получаем экземпляр Manager'а.
     *
     * @return object \MongoDB\Driver\Manager
     */
    public function initManager()
    {
        if (empty($this->manager)) {
            $server = $this->config->getConfigValue('Mongo', 'server');
            $port = $this->config->getConfigValue('Mongo', 'port');

            $this->manager = new \MongoDB\Driver\Manager('mongodb://' . $server . ':' . $port);
        }

        return $this;
    }

    /**
     * Создает объект запроса для чтения из базы.
     *
     * Возможные элементы атрибута $options:
     * limit        integer         The number of documents to be returned
     * batchSize    integer         The number of documents to return per batch
     * skip         integer         The number of documents to skip before returning
     * sort         array|object    The order in which to return matching documents
     * modifiers    array           Meta operators modifying the output or behavior of a query
     * projection   array|object    Specifies the fields to return using booleans or projection operators
     * tailable     bool            Cursor will not be closed when the last data is retrieved.
     *                              You can resume this cursor later
     * slaveOk      bool            Allow query of replica set secondaries
     * oplogReplay  bool            Internal MongoDB Server flag
     * noCursorTimeout     bool     Do not timeout a cursor that has been idle for more then 10minutes
     * awaitData    bool            Block rather than returning no data. After a period, time out.
     *                              Useful for tailable cursor
     * exhaust      bool            Stream the data down full blast in multiple "reply" packets.
     *                              Faster when you are pulling down a lot of data and you know you want to retrieve it all
     * partial      bool            Get partial results from mongos if some shards are down (instead of throwing an error)
     *
     * @param array $filter
     * @param array $option
     *
     * @return \MongoDB\Driver\Query
     */
    public function createQuery(array $filter = [], array $options = [])
    {
        $query = new \MongoDB\Driver\Query($filter, $options);

        return $query;
    }

    /**
     * Выполняем запрос.
     *
     * @param string $collection название коллекции в виде db_name.collection_name
     * @param \MongoDB\Driver\Query $query объект запроса
     *
     * @return \MongoDB\Driver\Cursor
     */
    public function executeQuery($collection, \MongoDB\Driver\Query $query)
    {
        return $this->manager->executeQuery($collection, $query);
    }

    /**
     * Вставляем запись в объект группового запроса.
     *
     * @param array $data запрос
     *
     * @return \MongoDB\Driver\BulkWrite
     */
    public function bulkInsert(array $data)
    {
        $bulk = $this->getBulk();

        $bulk->insert($data);

        return $bulk;
    }

    /**
     * Вставляем запись в объект группового запроса.
     *
     * @param array $data запрос
     *
     * @return \MongoDB\Driver\BulkWrite
     */
    public function remove(array $data = [])
    {
        $bulk = $this->getBulk();

        $bulk->delete($data);

        return $bulk;
    }

    /**
     * Получаем экземпляр объекта группового запроса.
     *
     * @return \MongoDB\Driver\BulkWrite
     */
    protected function getBulk()
    {
        if (empty($this->bulkWrite)) {
            $this->bulkWrite = new \MongoDB\Driver\BulkWrite;
        }

        return $this->bulkWrite;
    }

    /**
     * Выполняем групповой запрос.
     *
     * @return \MongoDB\Driver\WriteResult
     */
    public function executeBulkWrite($collection, \MongoDB\Driver\BulkWrite $query)
    {
        return $this->manager->executeBulkWrite($collection, $query);
    }

    /**
     * Вставляем один документ. Коммандой.
     *
     * @param array $data массив с информацией для вставки
     * @param string $collectionName название коллекции
     * @param string $db название базы
     *
     * @return object $this
     */
    public function insertOne(array $data, $collectionName, $db)
    {
        $this->command(
            $db,
            [
                'insert' => $collectionName,
                'documents' => [
                    $data
                ]
            ]
        );

        return $this;
    }

    /**
     * Обновляем один документ. Коммандой.
     *
     * {
     *    update: "users",
     *    updates: [
     *       {
     *         q: { user: "abc123" }, u: { $set: { status: "A" }, $inc: { points: 1 } }
     *       }
     *    ],
     *    ordered: false,
     *    writeConcern: { w: "majority", wtimeout: 5000 }
     * }
     *
     * @param array $query информация для поиска записей
     * @param array $updates информация для обновления
     * @param boolean $upsert если записей не найдено надо ли создавать новую?
     * @param boolean $multi обновляем одну или все найденные записи
     * @param string $collectionName название коллекции
     * @param string $db название базы
     *
     * @return array
     */
    protected function updateOne(
        array $query = [],
        array $updates = [],
        $upsert = false,
        $multi = false,
        $collectionName,
        $db
    ) {
        return $this->command(
            $db,
            [
                'update' => $collectionName,
                'updates' => [
                    [
                        'q' => $query,
                        'u' => [
                            '$set' => $updates
                        ],
                        'upsert' => $upsert,
                        'multi' => $multi
                    ]
                ]
            ]
        );
    }

    /**
     * Выполняем команду.
     *
     * @todo изменить на получение названия базы
     *
     * @param array $options команда
     *
     * @return array результат выполнения команды
     */
    protected function command($db, array $data)
    {
        $command = new \MongoDB\Driver\Command($data);

        $cursor = $this->manager->executeCommand($db, $command);

        return $cursor->toArray()[0];
    }
}
