<?php

namespace Millenium\Framework\Registry;

use Millenium\Framework;
use Millenium\Framework\Storage;

/**
 * Dependency injection.
 * Класс содержащий в себе все сущности проекта, которые могут быть созданы и вызваны.
 *
 * Реестр объектов.
 * Через него проходит вызов сущностей проекта из di.
 *
 * @package     Millenium\Framework
 * @author      Alexandr Golikov <my-millenium@yandex.ru>
 * @copyright   2016 Alexandr Golikov
 */
class Di
{

    /**
     * Экземпляр конфига.
     *
     * @var \Millenium\Framework\Config
     */
    protected $config;

    /**
     * Хранилище сущностей.
     *
     * @var array
     */
    private $storage = [];

    /**
     * Хранилище вызванных моделей.
     *
     * @var array
     */
    private $calledModels = [];

    /**
     * Инициализация всех сущностей проекта.
     *
     * @return [type] [description]
     */
    public function init()
    {
        $projectName = $this->config->getConfigValue('Project', 'name');
        $storagePath = $this->config->getConfigValue('Storage', 'path');
        $files = new \DirectoryIterator(__ROOT__ . DIRECTORY_SEPARATOR . $storagePath);

        foreach ($files as $file) {
            if (!$file->isFile() || $file->getExtension() != 'php') {
                continue;
            }

            $name = strstr($file->getBasename(), '.', true);
            $class = $projectName . '\\' . str_replace('/', '\\', $storagePath) . '\\' . $name;
            $object = new $class;
            $object->setConfig($this->config);
            $closure = $object->getClosure();
            $this->set($name, $closure);
        }

        return $this;
    }

    /**
     * Устанавливаем экземпляр конфига.
     *
     * @param \Millenium\Framework\Config $config
     */
    public function setConfig(Framework\Config $config)
    {
        $this->config = $config;

        return $this;
    }

    /**
     * Записывам сущность.
     *
     * @param string $name название сущности по которой ее можно вызвать
     * @param callable $callback функция обраатного вызова для сущности
     *
     * @return $this
     */
    public function set($name, $callback)
    {
        $name = strtolower($name);

        if (!isset($this->storage[$name])) {
            $this->storage[$name] = $callback;
        }

        return $this;
    }

    /**
     * Отдаем сущность.
     *
     * @param string $name имя запрошенной сущности
     * @param mixed $params параметры для передачи в сущность
     *
     * @return object запрошенная сущность
     */
    public function get($name, $params = null)
    {
        $name = strtolower($name);

        if (!isset($this->storage[$name])) {
            throw new \Millenium\Framework\Exceptions\Registry(
                'Unknown entity "' . $name . '".'
            );
        }

        $this->calledModels[$name] = $this->storage[$name]($params);

        return $this->calledModels[$name];
    }
}
