<?php

namespace Millenium\Framework\Registry;

/**
 *
 */
interface StorageInterface
{

    /**
     * [getClosure description]
     *
     * @return [type] [description]
     */
    public function getClosure();
}
