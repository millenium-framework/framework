<?php

namespace Millenium\Framework\Exceptions;

/**
 * Исключения конфига.
 *
 * @package     Millenium\Framework\Exception
 * @author      Alexandr Golikov <my-millenium@yandex.ru>
 * @copyright   2016 Alexandr Golikov
 */
class Config extends \Exception
{
}
