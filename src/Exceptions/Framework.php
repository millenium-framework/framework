<?php

namespace Millenium\Framework\Exceptions;

/**
 * Исключения реестра.
 *
 * @package     Millenium\Framework\Exception
 * @author      Alexandr Golikov <my-millenium@yandex.ru>
 * @copyright   2015 Alexandr Golikov
 */
class Framework extends \Exception
{
}
