<?php

namespace Millenium\Framework\Exceptions;

/**
 * Исключения баз данных.
 *
 * @package     Millenium\Framework\Exception
 * @author      Alexandr Golikov <my-millenium@yandex.ru>
 * @copyright   2015 Alexandr Golikov
 */
class Database extends \Exception
{
}
