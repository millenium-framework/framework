<?php

namespace Millenium\Framework\Exceptions;

/**
 * Исключение роутинга.
 *
 * @package     Millenium\Framework\Exception
 * @author      Alexandr Golikov <my-millenium@yandex.ru>
 * @copyright   2015 Alexandr Golikov
 */
class Routing extends \Exception
{
}
