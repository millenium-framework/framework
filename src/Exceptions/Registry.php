<?php

namespace Millenium\Framework\Exceptions;

/**
 * Основные исключения.
 *
 * @package     Millenium\Framework\Exception
 * @author      Alexandr Golikov <my-millenium@yandex.ru>
 * @copyright   2016 Alexandr Golikov
 */
class Registry extends \Exception
{
}
