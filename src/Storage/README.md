# Директория для сущностей проекта. #
Одна сущность - один файл.
Обрабатываются файлы только .php файлы.
Вариант для примера:

```php
namespace Millenium\Framework\Storage;

use \Millenium\Framework;
use \Millenium\Framework\Registry;

class Di extends Registry\Di implements Registry\StorageInterface
{

    public function getClosure()
    {
        return function() {
            $di = new Framework\Di;
            $di
                ->setConfig($this->config)
                ->init();

            return $di;
        };
    }
}
```
