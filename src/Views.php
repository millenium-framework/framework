<?php

namespace Millenium\Framework;

use \Symfony\Component\Yaml\Parser;

/**
 * Класс для работы со схемами.
 *
 * @package     Millenium\Framework
 * @author      Alexandr Golikov <my-millenium@yandex.ru>
 * @copyright   2016 Alexandr Golikov
 */
class Views extends Base
{

    /**
     * Схема.
     *
     * @var string
     */
    private $view;

    /**
     * Получаем чистую "как есть" схему.
     *
     * @return array
     */
    public function getRawView()
    {
        return $this->view;
    }

    /**
     * Получаем одно значение
     *
     * @param string $name название группы
     * @param string $value название значения
     *
     * @return array
     */
    public function getOneValue($name, $value = false)
    {
        if ($value == true) {
            return strtolower($this->view[$name][$value]);
        }

        return $this->view[ucfirst(strtolower($name))];
    }

    /**
     * Инициализация схемы.
     *
     * @param string $name название схемы
     *
     * @return object $this
     */
    public function initView($name, $category = 'Views')
    {
        $view = $this->getConfig()->getConfigValue($category, $name);

        $parser = new Parser();
        $this->view = $parser->parse(file_get_contents(__ROOT__ . $view));

        return $this;
    }

    /**
     * Получаем схему в рамках инициализации другой схемы.
     *
     * @param string $name название схемы
     *
     * @return array
     */
    public function getAnotherView($name)
    {
        $view = $this->getConfig()->getConfigValue('Views', $name);

        $parser = new Parser();
        return $parser->parse(file_get_contents(__ROOT__ . $view));
    }

    /**
     * Получаем обязательные поля из инициализированной схемы.
     *
     * @param string опциональное название схемы, если не указано, то берется инициализированная
     *
     * @return array
     */
    public function getRequiredFields($view = false)
    {
        if ($view != false) {
            $view = $this->view;
        }

        return $this->prepareViewForRequiredFields($view);
    }

    /**
     * Подготавливаем схему с обязательными полями.
     *
     * @param string $view название схемы
     *
     * @return array
     */
    private function prepareViewForRequiredFields($view)
    {
        $result = [];

        foreach ($view as $key => $value) {
            if (isset($value['required'])) {
                $result[$key] = $value['required'];
            }

            if (isset($value['type'])) {
                $subView = substr(strstr($value['type'], ':'), 1);

                if(!empty($subView)) {
                    $foo = $this->getRequiredFields($this->getAnotherView($subView));

                    if (!empty($foo)) {
                        $result[$key] = $foo;
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Получаем полностью готовое, со всеми наследниками, отображение схемы.
     *
     * @param string опциональное название схемы, если не указано, то берется инициализированная
     *
     * @return array
     */
    public function getCompleteView($view = false)
    {
        if ($view != false) {
            $view = $this->getAnotherView($view);
        } else {
            $view = $this->view;
        }

        return $this->prepareViewForComplete($view);
    }

    /**
     * Подготавливаем схему для полного отображения.
     *
     * @param string $view название схемы
     *
     * @return array
     */
    private function prepareViewForComplete($view)
    {
        foreach ($view as $key => $value) {
            if (isset($value['disable']) && $value['disable'] == true) {
                unset($view[$key]);
                continue;
            }

            if (isset($value['type'])) {
                $subView = substr(strstr($value['type'], ':'), 1);

                if(!empty($subView)) {
                    $view[$key]['type'] = $this->getCompleteView($this->getAnotherView($subView));
                }
            }
        }

        return $view;
    }
}
