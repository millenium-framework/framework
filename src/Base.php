<?php

namespace Millenium\Framework;

use \Millenium\Framework\Registry;
use \Millenium\Framework\Exceptions;

/**
 * Базовый класс для всех страниц проекта.
 *
 * @package     Millenium
 * @author      Alexandr Golikov <my-millenium@yandex.ru>
 * @copyright   2016 Alexandr Golikov
 */
abstract class Base implements BaseInterface, HttpInterface
{

    /**
     * Экземпляр шаблонизатора.
     */
    private $templater;

    /**
     * Экземпляр конфига.
     *
     * @var \Millenium\Framework\Config
     */
    private $config;

    /**
     * Экземпляр реестра.
     *
     * @var \Millenium\Framework\Registry
     */
    private $registry;

    /**
     * Заголовоки.
     *
     * @var array
     */
    private $header = [
        'Content-Type: text/html; charset=utf-8'
    ];

    /**
     * Информация для отображения полученная из запрошенного действия(метода).
     *
     * @var string
     */
    protected $data = '';

    /**
     * Инициализация приложения.
     *
     * @return $this
     */
    public function initApplication()
    {
        $action = $this->action;

        if (!method_exists($this, $action)) {
            throw new Exceptions\Framework(
                'Запрошенное действие "' . $action . '" объявлено, но не найдено'
            );
        }

        $this->beforeAction();
        $this->data = strval($this->$action());
        $this->sentHeaders();

        return $this;
    }

    /**
     * Действия перед запрошенным действием(методом).
     *
     * @return $this
     */
    abstract protected function beforeAction();

    /**
     * Посылатель заголовков.
     *
     * @return $this
     */
    public function sentHeaders()
    {
        if (!empty($this->headers)) {
            foreach ($this->headers as $header) {
                header($header);
            }
        }

        return $this;
    }

    /**
     * Сеттер кода ответа.
     *
     * @param integer $code код ответа
     *
     * @return $this
     */
    public function setResponceCode($code)
    {
        if ($code === intval($code)) {
            http_response_code($code);
        }

        return $this;
    }

    /**
     * Сеттер заголовков.
     * Передавать в чистом, должном виде.
     *
     * @param string $contentType заголовок
     *
     * @return $this
     */
    public function setHeader($header)
    {
        $this->headers[] = $header;

        return $this;
    }

    /**
     * Магический метод отдачи информации.
     *
     * @return string информация полученная из запрошенного метода.
     */
    public function __toString()
    {
        return $this->data;
    }

    /**
     * Устанавливаем действие(метод) полученный в роутинге для выполнения.
     *
     * @param string $action название действия(метода)
     *
     * @return $this
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Устанавливаем экземпляр реестра.
     *
     * @param \Millenium\Framework\Di $registry
     *
     * @return $this
     */
    public function setRegistry(Registry\Di $registry)
    {
        if (empty($this->registry)) {
            $this->registry = $registry;
        }

        return $this;
    }

    /**
     * Получаем экземпляр реестра.
     *
     * @return \Millenium\Framework\Registry
     */
    public function getRegistry()
    {
        return $this->registry;
    }

    /**
     * Устанавливаем экземпляр конфига.
     *
     * @param \Millenium\Framework\Config $config
     *
     * @return $this
     */
    public function setConfig(Config $config)
    {
        if (empty($this->config)) {
            $this->config = $config;
        }

        return $this;
    }

    /**
     * Получаем экземпляр конфига.
     *
     * @return \Millenium\Framework\Config\
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Возвращаем экземпляр шаблонизатора.
     * Если его не существует, то создаем новый.
     *
     * @return object
     */
    public function getTemplater()
    {
        if (empty($this->templater)) {
            $templaterName = $this->getConfig()->getConfigValue('Templater', 'name');
            $this->templater = $this->getRegistry()->get($templaterName);
        }

        return $this->templater;
    }
}
