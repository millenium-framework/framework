<?php

namespace Millenium\Framework;

/**
*
*/
interface HttpInterface
{
    public function sentHeaders();

    public function setHeader($header);

    public function setResponceCode($code);
}
