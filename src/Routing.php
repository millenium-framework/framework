<?php

namespace Millenium\Framework;

use \Symfony\Component\Yaml\Parser;

/**
 * Роутинг.
 *
 * @package     Millenium\Framework
 * @author      Alexandr Golikov <my-millenium@yandex.ru>
 * @copyright   2015 Alexandr Golikov
 */
class Routing
{

    /**
     * Экземпляр конфига.
     *
     * @var \Millenium\Framework\Config
     */
    private $config;

    /**
     * Конструктор.
     * Принимаем и устанавливаем конфиг.
     *
     * @param \Millenium\Framework\Config $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * Находим роут, отдаем класс соответствущий роуту.
     *
     * @throws \Millenium\Framework\Exceptions\Routing
     *
     * @return string
     */
    public function findRoute()
    {
        $url = $_SERVER['REQUEST_URI'];
        $url = str_replace('%20', ' ', $url);

        // Обрезаем GET - он нам не нужен
        if (strpos($url, '?')) {
            $url = strstr($url, '?', true);
        }

        $routes = $this->getRoutes();

        foreach ($routes as $route) {
            $routeUrl = '#^' . str_replace('/', '\/', $route['url']) . '/?$#i';

            if (preg_match($routeUrl, $url, $urlElements)) {
                if (class_exists($route['class'])) {
                    $object = new $route['class'];

                    if (isset($urlElements['action']) && method_exists($object, $urlElements['action'])) {
                        $object->setAction($urlElements['action']);
                    }

                    $this->config->setUrlVars($urlElements);

                    return $object;
                }

                throw new \Millenium\Framework\Exceptions\Routing(
                    'Route(' . $url . ') found, but class(' . $route['class'] . ') not exists.'
                );
            }
        }

        throw new \Millenium\Framework\Exceptions\Routing(
            'Route(' . $url . ') not found.'
        );
    }

    /**
     * Парсим файл роутинга.
     *
     * @return array
     */
    protected function getRoutes()
    {
        $configFile = $this->config->getConfigValue('Route', 'page');

        if (file_exists(__ROOT__ . $configFile)) {
            $parser = new Parser();

            return $parser->parse(file_get_contents(__ROOT__ . $configFile));
        }

        throw new \Millenium\Framework\Exceptions\Routing(
            'Routing file not found.'
        );
    }
}
